﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace SvAudit
{
	public partial class App : Application
	{
        public static string DB_PATH = string.Empty;

        public static string DB_SITUATION = string.Empty;

        public static User appUser = null;


		public App()
		{
			InitializeComponent();

			MainPage = new SvAudit.LoginPage();
		}

        public App(string DB_PATH_FROM_OS)
        {
            InitializeComponent();
            DB_PATH = DB_PATH_FROM_OS;
            MainPage = new SvAudit.LoginPage();

          

        }

        private static void CreateDataBase()
        {
            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {

                if (TableExists<User>(conn))
                {
                    DB_SITUATION = "table_exists";
                }
                else
                {
                  
                    DB_SITUATION = "table_doesnt_existed";
                    conn.CreateTable<User>();
                    conn.CreateTable<Audit>();
                    conn.CreateTable<AuditItem>();
                    conn.CreateTable<AuditItemMap>();

   

                }

            }
        }



        private static void PopulateDataBase()
        {
            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {

                if (TableExists<User>(conn))
                {

                    Audit audit1 = new Audit(1, "AUDITORIA X");
                    conn.Insert(audit1);

                    AuditItem ai1 = new AuditItem(1, "The work enviroment is clean and organized", "5S - HouseKeeping");
                    AuditItem ai2 = new AuditItem(2, "There are no materials on the floor", "5S - HouseKeeping");
                    AuditItem ai3 = new AuditItem(3, "The floor area is corretly demarcated", "5S - HouseKeeping");

                    AuditItem ai4 = new AuditItem(4, "The PPE'S are adequated to the task", "PPE's");
                    AuditItem ai5 = new AuditItem(5, "The PPEs are correctly allocated", "PPE's");

                    AuditItem ai6 = new AuditItem(6, "All eletricity panels are identified and protected", "Electricity");

                    AuditItemMap aim1 = new AuditItemMap(1, 1, true);
                    AuditItemMap aim4 = new AuditItemMap(1, 2, true);
                    AuditItemMap aim5 = new AuditItemMap(1, 3, true);
                    AuditItemMap aim6 = new AuditItemMap(1, 4, true);
                    AuditItemMap aim2 = new AuditItemMap(2, 1, true);
                    AuditItemMap aim3 = new AuditItemMap(3, 1, true);


                    conn.Insert(ai1);
                    conn.Insert(ai2);
                    conn.Insert(ai3);
                    conn.Insert(ai4);
                    conn.Insert(ai5);
                    conn.Insert(ai6);
                    


                    conn.Insert(aim1);
                    conn.Insert(aim2);
                    conn.Insert(aim3);
                    conn.Insert(aim4);
                    conn.Insert(aim5);
                    conn.Insert(aim6);
                }
               

            }
        }


        private static void DestroyDataBase()
        {
            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {

              
                    DB_SITUATION = "table_doesnt_existed";
                    conn.DropTable<User>();
                    conn.DropTable<Audit>();
                    conn.DropTable<AuditItem>();
                    conn.DropTable<AuditItemMap>();

                   
                

            }
        }

        protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}

        public static bool TableExists<T>(SQLiteConnection connection)
        {
            const string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            var cmd = connection.CreateCommand(cmdText, typeof(T).Name);
            return cmd.ExecuteScalar<string>() != null;
        }

    }

}
