﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SvAudit
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserAuditsPage : ContentPage
	{
        public static Audit selectedAudit;
		public UserAuditsPage()
		{
			InitializeComponent();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();

            userAuditsListView.ItemsSource = LoginPage.userAudits;
         
        }

        void HandleItemSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            Audit selected = (Audit) e.SelectedItem;
            DisplayAlert("Item Selected", selected.Name, "Ok");

            selectedAudit = selected;
            App.Current.MainPage = new MakeAuditPage();
            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }

    }
    


}