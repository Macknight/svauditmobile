﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SvAudit
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MakeAuditPage : ContentPage
    {
        public MakeAuditPage()
        {
            InitializeComponent();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();

            int currentAuditId = UserAuditsPage.selectedAudit.Id;

            using (SQLite.SQLiteConnection conn = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                //get the code of all itensId in the selected audit
                List<AuditItemMap> auditItensMap = conn.Query<AuditItemMap>("SELECT * FROM AUDITITEMMAP WHERE AUDITID = ?", currentAuditId);

                //holds every singular non-repeating id
                HashSet<int> auditItensIds = new HashSet<int>();
                foreach (AuditItemMap item in auditItensMap)
                {
                    auditItensIds.Add(item.AuditItemId);
                }
                string query = "SELECT * FROM AUDITITEM WHERE ID IN (";
                foreach (int auditItemId in auditItensIds)
                {
                  
                    if (!(auditItensIds.ElementAt(auditItensIds.Count - 1) == auditItemId))
                    {
                        query += auditItemId + ",";
                    }
                    else
                    {
                        query += auditItemId;
                    }
                }
                query += ")";
                List<AuditItem> questions = conn.Query<AuditItem>(query);


                DisplayAlert("dbSituation", "dbSitiation", App.DB_SITUATION);
                currentAuditListView.ItemsSource = questions;
            }
           

        }


        IEnumerable<string> GetMyOptions()
        {

              
            return new[] { "Ok", "Não ok", "My Option3" };
        }
    }
}