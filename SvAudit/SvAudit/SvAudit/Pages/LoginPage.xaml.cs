﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SvAudit
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{

        public static List<Audit> userAudits;
		public LoginPage()
		{
			InitializeComponent();


		}

		private void Button_Clicked(object sender, EventArgs e)
		{

            User test = new User(0, "james", "teste", 1);
            string name = userNameEntry.Text;
            string password = passwordEntry.Text;
         
            using (SQLite.SQLiteConnection conn  = new SQLite.SQLiteConnection(App.DB_PATH))
            {
                userAudits = conn.Table<Audit>().ToList();
                
                DisplayAlert("dbSituation", "dbSitiation", App.DB_SITUATION );
            }
            
			App.Current.MainPage = new UserAuditsPage();
			
		}
	}
}