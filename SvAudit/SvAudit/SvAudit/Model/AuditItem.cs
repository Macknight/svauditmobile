﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvAudit
{
    public class AuditItem
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Category { get; set; }

        public AuditItem()
        {
            this.Id = 0;
            this.Question = "no question";
            this.Category = "no category";

        }

        public AuditItem(int id, string question, string category)
        {
            this.Id = id;
            this.Question = question;
            this.Category = category;

        }
    }
}
