﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SvAudit
{
    public class Audit
    {
        public Audit()
        {
            this.Id = 0;
            this.Name = "noName";
        }

        public Audit(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }

       
    }
}
