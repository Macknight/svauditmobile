﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvAudit
{
    class AuditItemMap
    {
        public int AuditId { get; set; }
        public int AuditItemId { get; set; }
        public bool Required { get; set; }


        public AuditItemMap()
        {
            this.AuditId = 0;
            this.AuditItemId = 0;
            this.Required = false;
        }

        public AuditItemMap(int auditId, int auditItemId, bool required)
        {
            this.AuditId = auditId;
            this.AuditItemId = auditItemId;
            this.Required = required;
        }

    }
}
