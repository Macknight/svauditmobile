﻿using System;
using Android.Views;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Widget;
using Android.OS;
using System.IO;

namespace SvAudit.Droid
{
	[Activity(Label = "SvAudit", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

            //defining the path of the database for an android OS
            string fileName = "SVAudit_db.sqlite"; //the fileName
            string fileLocation = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // the file location
            string filePath = Path.Combine(fileLocation, fileName); //combining both for the full path

			global::Xamarin.Forms.Forms.Init(this, bundle);
			LoadApplication(new App(filePath));


            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (toolbar != null) { 
                SetActionBar(toolbar);
                ActionBar.Title = "NissamAudit";
            }
        }
	}
}

